<?php

namespace App\Http\Controllers;

use App\Http\Traits\Hashtags;
use App\Models\Fork;
use App\Models\Hashtag;
use App\Models\Post;
use App\Models\User;
use Inertia\Inertia;
use Symfony\Component\HttpFoundation\Response;

class PostController extends Controller
{
    use Hashtags;

    public function show(Post $post) {
        if (!auth()->user()->owns($post)) {
            abort(Response::HTTP_FORBIDDEN);
        }
        
        return Inertia::render('Post/PostShow', ['post' => $post]);
    }

    public function delete(Post $post) {
        if (!auth()->user()->owns($post))
            abort(Response::HTTP_FORBIDDEN);

        $post->fullDelete();
    }

    public function ping(Post $post) {
        $post->ping();
    }

    public function unping(Post $post) {
        $post->unping();
    }

    public function fork(Post $post) {
        request()->validate([
            'description' => ['nullable', 'max:5000']
        ]);

        $post->fork(request('description'));
    }


    public function deleteFork($id) {
        if (Fork::find($id)->user_uuid != auth()->id()) {
            abort(Response::HTTP_FORBIDDEN);
        }

        Fork::find($id)->delete();

        return redirect()->back();
    }

    public function indexApi() {
        $pageSize = request('pageSize') ?? env('PAGE_SIZE');

        $followingIds = auth()->user()->following()->where('approved', true)->pluck('uuid')->toArray();
        $followingIds[] = auth()->id();
        $posts = Post::latest()->whereIn('user_uuid', $followingIds)->paginate($pageSize);
        $forks = Fork::whereIn('user_uuid', $followingIds)->paginate($pageSize);

        $aggregate = collect([]);
        $aggregate = $aggregate->concat($posts);
        $aggregate = $aggregate->concat($forks);
        $sorted = $aggregate->sortByDesc(fn ($item) => $item->created_at);

        $totalPages = (int) ceil((Post::latest()->whereIn('user_uuid', $followingIds)->count() + Fork::whereIn('user_uuid', $followingIds)->count()) / $pageSize);
        return ['items' => $sorted->values()->all(), 'count' => $totalPages];
    }

    public function profileApi($user = null) {
        if ($user != null) {
            $user = User::where('nickname', $user)->first();

            if ($user == null) {
                abort(Response::HTTP_NOT_FOUND);
            }
            
            if (auth()->user()->isPrivate()) {
                $empty = ['items' => collect([]), 'count' => 0];
                if (!auth()->user()->follows($user)) {
                    return $empty;
                }

                if (!$user->approves(auth()->user())) {
                    return $empty;
                }
            }
        }
        
        $pageSize = request('pageSize') ?? env('PAGE_SIZE');

        $userId = $user ? $user->uuid : auth()->id();
        $user = $user ? $user : auth()->user();

        $posts = $user->posts()->paginate($pageSize);
        $forks = Fork::where('user_uuid', $userId)->paginate($pageSize);

        $aggregate = collect([]);
        $aggregate = $aggregate->concat($posts);
        $aggregate = $aggregate->concat($forks);

        $sorted = $aggregate->sortByDesc(fn ($item) => $item->created_at);

        $totalPages = (int) ceil(($user->posts()->count() + $user->forks()->count()) / $pageSize);
        return ['items' => $sorted->values()->all(), 'count' => $totalPages];
    }

    public function hashtagApi($hashtag) {
        $pageSize = request('pageSize') ?? env('PAGE_SIZE');

        $foundHashtag = Hashtag::where('name', $hashtag)->first();
        if ($foundHashtag == null) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $posts = $foundHashtag->posts()->paginate($pageSize);

        $aggregate = collect([]);
        $aggregate = $aggregate->concat($posts);

        $sorted = $aggregate->sortByDesc(fn ($item) => $item->created_at);
        $totalPages = (int) ceil($foundHashtag->posts()->count() / $pageSize);
        
        return ['items' => $sorted->values()->all(), 'count' => $totalPages];
    }
}
