<?php

namespace App\Http\Controllers\Settings\Appearance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaginationController extends Controller
{
    public function update() {
        auth()->user()->togglePagination(request('infinity') == '1' ? 1 : 0);

        return redirect()->back();
    }
}
