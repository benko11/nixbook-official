<?php

namespace App\Http\Controllers;

use App\Models\CodeLanguage;
use App\Models\Fork;
use App\Models\Post;
use Inertia\Inertia;

class HomeController extends Controller
{
    public function index()
    {
        $followingIds = auth()->user()->following()->where('approved', true)->pluck('uuid')->toArray();
        $followingIds[] = auth()->id();
        $posts = Post::latest()->whereIn('user_uuid', $followingIds)->paginate(15);
        $forks = Fork::whereIn('user_uuid', $followingIds)->paginate(15);
        $codeLanguages = CodeLanguage::get();

        $aggregate = collect([]);
        $aggregate = $aggregate->concat($posts);
        $aggregate = $aggregate->concat($forks);

        return Inertia::render('Home/Home', [
            'posts' => $aggregate->sortBy(fn ($item) => $item->created_at)->toArray(),
            'stashes' => auth()->user()->load('stashes')->stashes,
            'codeLanguages' => $codeLanguages
        ]);
    }
}
