<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Traits\Hashtags;
use App\Models\Hashtag;
use App\Models\Post;

class TextController extends Controller
{
    use Hashtags;

    public function store() {
        request()->validate([
            'message' => ['required']
        ], [
            'message.required' => 'Please type in the message'
        ]);

        $post = auth()->user()->addTextPost(request('message'));

        $this->addHashtags(request('message'), $post);

        return redirect()->back();
    }

    public function update(Post $post) {
        request()->validate([
            'contents' => ['required'],
        ], [
            'contents.required' => 'Please type in the message'
        ]);

        auth()->user()->updateTextPost($post, request('contents'));

        return redirect()->back();
    }
}
