<?php

namespace App\Http\Traits;

use App\Models\Hashtag;
use App\Models\Post;

trait Hashtags {
    public function addHashtags(string $message, Post $post) {
        $pattern = '/#[a-zA-Z0-9]+(?!\w)/';
        preg_match_all($pattern, $message, $matches);
        $hashtags = $matches[0];

        foreach ($hashtags as $hashtag) {
            $hashtag = ltrim($hashtag, '#');
            $foundHashtag = Hashtag::where('name', $hashtag)->first();
            if ($foundHashtag == null) {
                auth()->user()->addHashtag($hashtag);
                $foundHashtag = Hashtag::where('name', $hashtag)->first();
            }

            $post->hashtags()->attach($foundHashtag->id);
        }
    }
}