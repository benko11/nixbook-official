<?php

namespace App\Jobs;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteTrashedUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $currentTimestamp = date('Y-m-d H:i:s');
        
        $trashed = User::onlyTrashed()->get();
        foreach ($trashed as $user) {
            $deletedTimestamp = Carbon::parse($user->deleted_at)->format('Y-m-d H:i:s');
            $newTimestamp = date('Y-m-d H:i:s', strtotime($deletedTimestamp) + config('app.grace_period'));
            
            if (Carbon::parse($currentTimestamp)->gt(Carbon::parse($newTimestamp))) {
                $user->forceDelete();
            }
        }
    }
}
