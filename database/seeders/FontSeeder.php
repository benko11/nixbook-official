<?php

namespace Database\Seeders;

use App\Models\Font;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FontSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fonts = [
            ['name' => 'MS-DOS', 'url' => 'Perfect_DOS_VGA_437_Win.ttf'],
            ['name' => 'Fira Code', 'url' => 'FiraCode-VariableFont_wght.ttf'],
            ['name' => 'Retro IBM', 'url' => 'Ac437_Acer_VGA_8x8.ttf'],
            ['name' => 'ZX Spectrum', 'url' => 'zx-spectrum.ttf'],
            ['name' => 'Unifont', 'url' => 'GnuUnifontFull-Pm9P.ttf'],
            ['name' => 'Old Nokia', 'url' => 'nokiafc22.ttf'],            

            // ['name' => 'IBM Plex Mono'],
            // ['name' => 'Roboto Mono'],
            // ['name' => 'Source Code Pro'],
        ];

        foreach ($fonts as $font) {
            Font::create($font);
        }
    }
}
