<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach ($users as $user) {
            $rand = mt_rand(1, 100);

            if ($rand >= 80) continue;

            $count = 1;
            
            if ($user->isAdmin()) {
                $count = 100;
            }

            for ($i = 0; $i < $count; $i++) {
                $user->addTextPost($count > 1 ? 'Post '. $i : fake()->paragraph());
            }
        }
    }
}
