import React from "react";
import { useState } from "react";
import InteractiveFeed from "./InteractiveFeed";
import { useEffect } from "react";
import axios from "axios";
import Button from "@/Styles/Button";
import { useContext } from "react";
import { PreferencesContext } from "@/Layouts/AuthenticatedLayout";
import { usePreferences } from "@/hooks/usePreferences";
import styled from "styled-components";

export const UpdatedFeed = React.createContext(false);

const Pagination = ({ auth, stashes, emptyMessage, url, forms }) => {
    const preferences = useContext(PreferencesContext);
    const isInfinity = usePreferences(preferences, "infinite-scrolling");

    const [aggregate, setAggregate] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [updatedFeed, setUpdatedFeed] = useState(false);
    const [totalPages, setTotalPages] = useState(-1);

    useEffect(() => {
        fetchData(currentPage);

        if (updatedFeed) {
            setUpdatedFeed(false);
            setCurrentPage(1);
        }
    }, [currentPage, updatedFeed]);

    const updateFeed = (value) => setUpdatedFeed(value);

    const fetchData = async (page) => {
        try {
            const response = await axios.get(`${url}?page=${page}`);
            const { items, count } = response.data;

            console.log();
            let clone;
            if (isInfinity) {
                clone = page > 1 ? [...aggregate, ...items] : [...items];
            } else {
                clone = [...items];
            }
            setAggregate(clone);
            setTotalPages(count);
        } catch (err) {
            console.error(err);
        }
    };

    const renderShowMore = () => {
        if (isInfinity) {
            if (totalPages > currentPage)
                return (
                    <Button
                        className="mt-1"
                        style={{ width: "100%" }}
                        onClick={() => setCurrentPage((prev) => prev + 1)}
                    >
                        Load more
                    </Button>
                );

            if (aggregate.length > 0)
                return (
                    <div className="mt-4" style={{ textAlign: "center" }}>
                        Looks like you reached the end
                    </div>
                );
        }

        const links = [];
        for (let i = 1; i <= totalPages; i++) {
            if (totalPages === 1) break;
            links.push(
                <PageLink
                    key={i}
                    className={currentPage === i && "active"}
                    onClick={() => setCurrentPage(i)}
                >
                    {i}
                </PageLink>
            );
        }

        return (
            <div className="mt-2" style={{ display: "flex" }}>
                {currentPage > 1 && (
                    <Button onClick={() => setCurrentPage((prev) => prev - 1)}>
                        &lt; Previous
                    </Button>
                )}
                <div
                    style={{
                        flex: 1,
                        textAlign: "center",
                        alignSelf: "center",
                        flexDirection: "column-reverse",
                        justifySelf: "center",
                    }}
                >
                    {links.map((l) => l)}
                </div>
                {currentPage < totalPages && (
                    <Button
                        className="ml-auto"
                        onClick={() => setCurrentPage((prev) => prev + 1)}
                    >
                        Next &gt;
                    </Button>
                )}
            </div>
        );
    };

    return (
        <UpdatedFeed.Provider value={{ updateFeed }}>
            {forms != null && forms}
            <InteractiveFeed
                posts={aggregate}
                auth={auth}
                stashes={stashes}
                emptyMessage={emptyMessage}
            />
            {renderShowMore()}
        </UpdatedFeed.Provider>
    );
};

export default Pagination;

const PageLink = styled.div`
    padding: 0.5rem 1rem;
    cursor: pointer;
    display: inline-block;

    &.active {
        background: var(--primary-colour);
        color: var(--foreground-colour-dark);
    }
`;
