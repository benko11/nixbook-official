import React from "react";
import Modal from "../Modal";
import styled from "styled-components";
import TextInput from "../TextInput";
import Button from "@/Styles/Button";
import Form from "@/Styles/Form";
import { Inertia } from "@inertiajs/inertia";
import { useState } from "react";

export default function EditFactModal({
    show,
    onClose,
    userFacts,
    setEditAction,
}) {
    const [categories, setCategories] = useState(() => {
        return userFacts.map((uf) => uf.category);
    });

    const [values, setValues] = useState(() => {
        return userFacts.map((uf) => uf.pivot.value);
    });

    const handleEdit = (e) => {
        e.preventDefault();

        Inertia.put(
            route("profile.edit-facts", { categories, values }),
            {},
            {
                onSuccess: () => {
                    onClose();
                    setEditAction(true);
                },
            }
        );
    };

    return (
        <Modal show={show} onClose={onClose}>
            <div className="mb-2">
                To remove a metadata item, simply remove text from both fields.
            </div>

            <Form gap="0.5rem" onSubmit={handleEdit}>
                {userFacts.map((c, index) => (
                    <React.Fragment key={c.id}>
                        <Helper>
                            <TextInput
                                autoFocus={index === 0}
                                type="text"
                                name="category"
                                value={categories[index]}
                                onChange={(e) => {
                                    const clone = [...categories];
                                    clone[index] = e.target.value;
                                    setCategories(clone);
                                }}
                            />
                            <TextInput
                                type="text"
                                name="value"
                                value={values[index]}
                                onChange={(e) => {
                                    const clone = [...values];
                                    clone[index] = e.target.value;
                                    setValues(clone);
                                }}
                            />
                        </Helper>
                    </React.Fragment>
                ))}

                <Button>Edit metadata</Button>
            </Form>
        </Modal>
    );
}

const Helper = styled.div`
    display: flex;
    margin: 0.2rem 0;
    gap: 0.5rem;

    * {
        width: 100%;
    }
`;
