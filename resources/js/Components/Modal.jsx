import ModalWindow from "@/Styles/ModalWindow";
import Content from "@/Components/Content";
import Button from "@/Styles/Button";

export default function Modal({ title, show, children, onClose }) {
    if (!show) return null;

    return (
        <ModalWindow className="modal">
            <Content size="700px">
                <div
                    style={{
                        maxHeight: "80vh",
                        overflow: "auto",
                    }}
                >
                    <div
                        style={{
                            maxWidth: "600px",

                            backgroundColor: "var(--tertiary-colour)",
                            padding: "1.5rem 2rem",
                            boxShadow:
                                "20px 20px 0 var(--foreground-colour-dark)",
                        }}
                    >
                        {children}
                    </div>

                    <Button
                        colour="var(--tertiary-colour)"
                        onClick={onClose}
                        className="mt-4"
                        style={{
                            color: "var(--foreground-colour)",
                        }}
                    >
                        Close
                    </Button>
                </div>
            </Content>
        </ModalWindow>
    );
}
