import { useState } from "react";
import { FlashMessage } from "./FlashMessage";
import PostFeed from "./Post/PostFeed";
import { useContext } from "react";
import { PreferencesContext } from "@/Layouts/AuthenticatedLayout";
import { usePreferences } from "@/hooks/usePreferences";
import { useSide } from "@/hooks/useSide";
import BottomNavigationSettings from "./Post/BottomNavigationSettings";
import { Inertia } from "@inertiajs/inertia";
import { useEffect } from "react";
import { getPostContentColumn } from "@/utils/getPostContentColumn";
import ForkModal from "./Post/ForkModal";
import { DeleteForkModal } from "./Post/DeleteForkModal";
import EditModal from "./Post/EditModal";
import DeleteModal from "./Post/DeleteModal";
import BottomNavigationForkSettings from "./Post/BottomNavigationForkSettings";
import { UpdatedFeed } from "./Pagination";

export default function InteractiveFeed({
    posts,
    auth,
    stashes,
    emptyMessage,
}) {
    const { updateFeed } = useContext(UpdatedFeed);

    const [clipboard, setClipboard] = useState("");

    const [selection, setSelection] = useState(-1);

    const [message, setMessage] = useState("");

    const preferences = useContext(PreferencesContext);
    const flashSide = useSide(
        usePreferences(preferences, "flash-message-side")
    );
    const flashLength = usePreferences(preferences, "flash-message-length");

    const [isPinged, setIsPinged] = useState(false);
    const [isCopied, setIsCopied] = useState(false);
    const [isStashed, setIsStashed] = useState(false);

    const [showEditModal, setShowEditModal] = useState(false);
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [showForkModal, setShowForkModal] = useState(false);
    const [showDeleteForkModal, setShowDeleteForkModal] = useState(false);

    const [editAction, setEditAction] = useState(false);
    const [deleteAction, setDeleteAction] = useState(false);
    const [forkAction, setForkAction] = useState(false);
    const [forkDeleteAction, setForkDeleteAction] = useState(false);

    useEffect(() => {
        const timeout = setTimeout(() => setMessage(""), flashLength);
        return () => clearTimeout(timeout);
    }, [message]);

    const isFork = (order = null) => {
        let value = order == null ? selection : order;
        if (value == null || posts[value] == null) return false;
        return Object.hasOwn(posts[value], "post");
    };

    useEffect(() => {
        const copyToClipboard = async () => {
            try {
                await navigator.clipboard.writeText(clipboard);
            } catch {
                setMessage("Failed to copy to clipboard");
            }
        };

        if (isCopied) copyToClipboard();
    }, [isCopied, clipboard]);

    useEffect(() => {
        if (!editAction) return;

        setMessage("Post edited");
        setSelection(-1);
        setEditAction(false);
    }, [editAction]);

    useEffect(() => {
        if (!deleteAction) return;

        setMessage("Post deleted");
        setSelection(-1);
        setDeleteAction(false);
    }, [deleteAction]);

    useEffect(() => {
        if (!forkAction) return;

        setSelection(-1);
        setMessage("Post forked, you can view it in your profile");
        setForkAction(false);
    }, [forkAction]);

    useEffect(() => {
        if (!forkDeleteAction) return;

        setSelection(-1);
        setMessage("Fork deleted");
        setForkDeleteAction(false);
    }, [forkDeleteAction]);

    useEffect(() => {
        const closeModal = (e) => {
            if (e.key.toLowerCase() === "escape") {
                setShowEditModal(false);
                setShowForkModal(false);
                setShowDeleteModal(false);
                setShowDeleteForkModal(false);
            }
        };

        document.addEventListener("keydown", closeModal);

        return () => {
            document.removeEventListener("keydown", closeModal);
        };
    }, []);

    const handlePing = () => {
        setSelection(-1);

        if (isPinged)
            Inertia.post(
                route("posts.unping", { id: getPost().id }),
                {},
                {
                    preserveScroll: true,
                    onSuccess: () => {
                        setMessage("Post unpinged");
                        updateFeed(true);
                    },
                }
            );
        else
            Inertia.post(
                route("posts.ping", { id: getPost().id }),
                {},
                {
                    preserveScroll: true,
                    onSuccess: () => {
                        setMessage("Post pinged");
                        updateFeed(true);
                    },
                }
            );
    };

    const handleStash = (e) => {
        e.preventDefault();

        const filteredPost = posts[selection];

        const resetAll = () => {
            setIsStashed(false);
            setSelection(-1);
        };

        if (isStashed) {
            Inertia.delete(
                route("posts.stash-delete", {
                    id: filteredPost.id,
                    type: "post",
                }),
                {
                    preserveScroll: true,
                    onSuccess: () => {
                        resetAll();
                        setMessage("Post unstashed");
                        updateFeed(true);
                    },
                }
            );
            return;
        }

        Inertia.post(
            route("posts.stash", { id: filteredPost.id, type: "post" }),
            {},
            {
                preserveScroll: true,
                onSuccess: () => {
                    resetAll();
                    setMessage("Post stashed");
                    updateFeed(true);
                },
            }
        );
    };

    const handlePostSelect = (order) => {
        if (order === selection) setSelection(-1);
        else setSelection(order);

        let filteredPost;
        filteredPost = posts[order];
        if (isFork(order)) filteredPost = filteredPost.post;

        const foundPinged = isFork(order)
            ? false
            : filteredPost.pings.filter((item) => {
                  return (
                      item.user_uuid === auth.user.uuid &&
                      item.post_id === posts[order].id
                  );
              }).length > 0;
        setIsPinged(foundPinged);

        const stashesIds = stashes.map((item) => {
            return { id: item.stashable_id, type: item.stashable_type };
        });
        const stashesPostIds = stashesIds
            .filter((item) => item.type === "post")
            .map((item) => item.id);
        const foundStashed = stashesPostIds.includes(filteredPost.id);
        setIsStashed(foundStashed);
    };

    const handleFork = () => setShowForkModal(true);
    const handleEdit = () => setShowEditModal(true);
    const handleDelete = () => {
        setShowDeleteModal(true);
        setDeleteAction(false);
    };
    const handleForkDelete = () => setShowDeleteForkModal(true);
    const getPost = () => {
        if (selection == null || selection === -1) return {};
        if (posts[selection] != null) {
            if (isFork()) {
                return posts[selection].post;
            }
        }

        return posts[selection];
    };

    const handleCopy = () => {
        const post = isFork() ? posts[selection].post : posts[selection];
        const postType = post.postable_type;
        const type = getPostContentColumn(postType);

        setClipboard(post.postable[type]);
        setIsCopied(true);
        setSelection(-1);
        setMessage("Text from the post copied to clipboard");
    };

    const renderGlobalModals = () => {
        return (
            <>
                <ForkModal
                    show={showForkModal}
                    onClose={() => setShowForkModal(false)}
                    post={getPost()}
                    setForkAction={setForkAction}
                />

                <DeleteForkModal
                    show={showDeleteForkModal}
                    onClose={() => setShowDeleteForkModal(false)}
                    fork={posts[selection]}
                    setForkDeleteAction={setForkDeleteAction}
                />
            </>
        );
    };

    const renderPersonalModals = () => {
        if (selection == null) return null;

        return (
            <>
                <EditModal
                    show={showEditModal}
                    onClose={() => setShowEditModal(false)}
                    post={getPost()}
                    setEditAction={setEditAction}
                />

                <DeleteModal
                    show={showDeleteModal}
                    onClose={() => setShowDeleteModal(false)}
                    post={getPost()}
                    setDeleteAction={setDeleteAction}
                />
            </>
        );
    };

    return (
        <>
            <PostFeed
                posts={posts}
                selection={selection}
                onSelect={handlePostSelect}
                emptyMessage={emptyMessage}
            />

            {isFork() ? (
                <BottomNavigationForkSettings
                    selection={selection}
                    isPublic={
                        auth.user.uuid !== posts?.[selection]?.user_uuid ||
                        false
                    }
                    onCopy={handleCopy}
                    onDelete={handleForkDelete}
                />
            ) : (
                <BottomNavigationSettings
                    selection={selection}
                    pingCount={getPost()?.pings?.length}
                    isPublic={
                        auth.user.uuid !== posts?.[selection]?.user_uuid ||
                        false
                    }
                    isPinged={isPinged}
                    isStashed={isStashed}
                    onPing={handlePing}
                    onCopy={handleCopy}
                    onStash={handleStash}
                    onFork={handleFork}
                    onEdit={handleEdit}
                    onDelete={handleDelete}
                />
            )}

            <FlashMessage message={message} side={flashSide} />

            {renderGlobalModals()}
            {renderPersonalModals()}
        </>
    );
}
