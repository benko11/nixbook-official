import styled from "styled-components";

export default function Emoji({ block = false, character = "🏳️‍🌈" }) {
    if (block) return <EmojiBlock>{character}</EmojiBlock>;

    return <EmojiContainer>{character}</EmojiContainer>;
}

const EmojiBlock = styled.div`
    font-family: "Noto Emoji";
`;

const EmojiContainer = styled.span`
    font-family: "Noto Emoji";
`;
