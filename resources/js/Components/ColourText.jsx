import { useEffect } from "react";
import { useState } from "react";

export default function ColourText({ text, colours }) {
    const [encompassingClass, setEncompassingClass] = useState("");
    useEffect(() => {
        setEncompassingClass(crypto.randomUUID());
    }, []);

    const generateDynamicStyles = () => {
        if (encompassingClass === "") return;

        return colours.map((colour, index) => (
            <style key={index}>
                {`.${encompassingClass} span:nth-child(${colours.length}n + ${
                    index + 1
                }) {
                    color: ${colour};
                }`}
            </style>
        ));
    };

    const [splitText, setSplitText] = useState("");
    useEffect(() => {
        setSplitText(() =>
            text.split("").map((item, index) => <span key={index}>{item}</span>)
        );
    }, []);

    return (
        <>
            {generateDynamicStyles()}
            <span className={encompassingClass}>{splitText}</span>
        </>
    );
}
