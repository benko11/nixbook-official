import Modal from "@/Components/Modal";
import { Inertia } from "@inertiajs/inertia";
import Button from "../../Styles/Button";
import { useContext } from "react";
import { UpdatedFeed } from "../Pagination";

export function DeleteForkModal({ show, onClose, fork, setForkDeleteAction }) {
    const { updateFeed } = useContext(UpdatedFeed);

    const handleDeleteFork = (e) => {
        e.preventDefault();

        Inertia.delete(route("posts.fork-delete", { id: fork.id }), {
            onSuccess: () => {
                onClose();
                setForkDeleteAction(true);
                updateFeed(true);
            },
        });
    };

    return (
        <Modal show={show} onClose={onClose}>
            <form onSubmit={handleDeleteFork}>
                <Button>Delete</Button>
            </form>
        </Modal>
    );
}
