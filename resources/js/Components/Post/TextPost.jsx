import { Link } from "@inertiajs/inertia-react";
import Post from "./Post";
import { useContext } from "react";
import { PreferencesContext } from "../../Layouts/AuthenticatedLayout";
import { usePreferences } from "../../hooks/usePreferences";
import { formatContent } from "@/utils/formatContent";

export default function TextPost({ className, item, checked, onSelect, id }) {
    const preferences = useContext(PreferencesContext);
    const maxSize = usePreferences(preferences, "max-post-size");

    const renderPost = () => {
        const parts = formatContent(item.postable.contents);
        if (maxSize > 0)
            return (
                <div
                    style={{
                        maxHeight: `${maxSize}px`,
                        overflowY: "auto",
                    }}
                >
                    {parts}
                </div>
            );
        return parts;
    };

    const timestamp = item.human_at;
    const { nickname: owner } = item.owner;

    return (
        <Post id={id} checked={checked}>
            <div className={`${className} p-2 selection`} onClick={onSelect}>
                {renderPost()}
                <div
                    style={{
                        display: "flex",
                    }}
                >
                    <div>
                        <small>
                            {timestamp}{" "}
                            {item.created_at != item.updated_at && (
                                <em>(edited)</em>
                            )}
                        </small>
                    </div>
                    <div className="ml-auto">
                        <small>
                            by <Link href={`/profile/~${owner}`}>{owner}</Link>
                        </small>
                    </div>
                </div>
            </div>
        </Post>
    );
}
