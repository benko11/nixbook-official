import BottomNavigation from "@/Styles/BottomNavigation";

export default function BottomNavigationForkSettings({
    selection,
    isPublic,
    onCopy,
    onDelete,
}) {
    if (selection < 0) return null;

    return (
        <BottomNavigation style={{ bottom: 0, left: 0, zIndex: 1000 }}>
            <div
                className="ml-auto"
                style={{
                    display: "flex",
                    gap: ".75rem",
                }}
            >
                <div onClick={onCopy} style={{ margin: "1px 2px" }}>
                    Copy
                </div>

                {!isPublic && (
                    <>
                        <div onClick={onDelete} style={{ margin: "1px 2px" }}>
                            Delete
                        </div>
                    </>
                )}
            </div>
        </BottomNavigation>
    );
}
