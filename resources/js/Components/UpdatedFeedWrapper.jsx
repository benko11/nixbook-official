import { useState } from "react";

export const UpdatedFeed = React.createContext(false);

export const UpdatedFeedWrapper = ({ children }) => {
    const [updatedFeed, setUpdatedFeed] = useState(false);

    const updateFeed = (value) => setUpdatedFeed(value);

    return (
        <UpdatedFeed.Provider value={{ isUpdated: updatedFeed, updateFeed }}>
            {children}
        </UpdatedFeed.Provider>
    );
};
