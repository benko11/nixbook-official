import Container from "@/Styles/Container";

export default function Content({ size = "800px", children, className }) {
    return (
        <Container className={className}>
            <div style={{ width: size, maxWidth: "98%" }}>{children}</div>
        </Container>
    );
}
