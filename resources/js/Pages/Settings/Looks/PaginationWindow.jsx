import { FlashMessage } from "@/Components/FlashMessage";
import Checkbox from "@/Components/Forms/Checkbox";
import Window from "@/Components/Window";
import { PreferencesContext } from "@/Layouts/AuthenticatedLayout";
import Button from "@/Styles/Button";
import Form from "@/Styles/Form";
import { usePreferences } from "@/hooks/usePreferences";
import { useSide } from "@/hooks/useSide";
import { Inertia } from "@inertiajs/inertia";
import { useContext } from "react";
import { useEffect } from "react";
import { useState } from "react";

export default function PaginationWindow() {
    const preferences = useContext(PreferencesContext);
    const isInfinity = usePreferences(preferences, "infinite-scrolling");

    const [infinity, setInfinity] = useState(isInfinity);
    const [pageByPage, setPageByPage] = useState(!isInfinity);

    const [message, setMessage] = useState("");
    const flashLength = usePreferences(preferences, "flash-message-length");
    const flashSide = useSide(
        usePreferences(preferences, "flash-message-side")
    );

    useEffect(() => {
        const timeout = setTimeout(() => setMessage(""), flashLength);
        return () => clearTimeout(timeout);
    }, [message]);

    function handleSubmit(e) {
        e.preventDefault();

        Inertia.patch(
            route("settings.pagination-change", { infinity, pageByPage }),
            {},
            {
                preserveScroll: true,
                onSuccess: () => {
                    setMessage("Pagination settings applied");
                },
            }
        );
    }

    return (
        <Window title="Displaying posts" colour="var(--tertiary-colour)">
            <Form gap={0.25} onSubmit={handleSubmit}>
                <Checkbox
                    name="infinity"
                    value="infinity"
                    label="Infinite scrolling"
                    checked={infinity}
                    onChange={() => {
                        setInfinity(true);
                        setPageByPage(false);
                    }}
                />
                <Checkbox
                    name="pageByPage"
                    value="pageByPage"
                    label="Page by Page"
                    checked={pageByPage}
                    onChange={() => {
                        setPageByPage(true);
                        setInfinity(false);
                    }}
                />

                <Button className="mt-1">Apply</Button>
            </Form>

            <FlashMessage message={message} side={flashSide} />
        </Window>
    );
}
