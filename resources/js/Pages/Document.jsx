import GuestLayout from "@/Layouts/GuestLayout";
import { Head } from "@inertiajs/inertia-react";
import Content from "@/Components/Content";
import { ReactMarkdown } from "react-markdown/lib/react-markdown";
import Window from "@/Components/Window";
import styled, { useTheme } from "styled-components";
import { usePreferences } from "../hooks/usePreferences";
import { useColourToHex } from "../hooks/useColourToHex";
import { useContext, useEffect } from "react";
import { ThemeUpdateContext } from "../AppWrapper";
import AutomaticLayout from "@/Layouts/AutomaticLayout";

export default function Document({ auth, file, title }) {
    const preferences = auth?.user?.preferences;
    let lineHeight;
    try {
        lineHeight = usePreferences(preferences, "line-height") || 140;
    } catch {
        lineHeight = 140;
    }

    return (
        <AutomaticLayout auth={auth}>
            <Head title={title} />

            <Content size="800px">
                <div style={{ marginTop: "1rem", marginBottom: "4rem" }}>
                    <Window colour="var(--tertiary-colour)" title={title}>
                        <DocumentContainer lineHeight={lineHeight}>
                            <ReactMarkdown>{file}</ReactMarkdown>
                        </DocumentContainer>
                    </Window>
                </div>
            </Content>
        </AutomaticLayout>
    );
}

const DocumentContainer = styled.div`
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        padding-top: 0.5rem;
        padding-bottom: 0.5rem;
    }

    p {
        line-height: ${(props) => props.lineHeight || 140}%;
    }
`;
