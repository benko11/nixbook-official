import Content from "@/Components/Content";
import InteractiveFeed from "@/Components/InteractiveFeed";
import Pagination from "@/Components/Pagination";
import PostFeed from "@/Components/Post/PostFeed";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head } from "@inertiajs/inertia-react";

export default function Hashtag({ auth, errors, hashtag, stashes }) {
    return (
        <AuthenticatedLayout auth={auth} errors={errors}>
            <Head title={`#${hashtag}`} />
            <Content className="mt-2">
                <h2>Results for #{hashtag}</h2>
                <div style={{ marginBottom: "5rem" }}>
                    <Pagination
                        auth={auth}
                        stashes={stashes}
                        emptyMessage={
                            <div className="mt-2">
                                No posts found for #{hashtag}
                            </div>
                        }
                        url={`/api/posts/hashtag/${hashtag}`}
                    />
                </div>
            </Content>
        </AuthenticatedLayout>
    );
}
