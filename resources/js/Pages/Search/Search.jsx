import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import Content from "@/Components/Content";
import { Head, Link } from "@inertiajs/inertia-react";
import { useEffect, useState } from "react";
import TextInput from "@/Components/TextInput";
import axios from "axios";

export default function Search({ auth, errors, recentUsers, hashtags }) {
    const [sent, setSent] = useState(false);
    const [query, setQuery] = useState("");
    const [results, setResults] = useState({});
    const [foundHashtags, setFoundHashtags] = useState(false);

    const handleSearch = async (e) => {
        e.preventDefault();
        setSent(true);

        const isHashtag = query.trim().startsWith("#");
        const url = isHashtag
            ? `/api/search/hashtags/${query.substring(1)}`
            : `/api/search/users/${query}`;

        try {
            const raw = await axios.get(url);
            const { data } = raw;

            setResults(data);
            if (isHashtag) setFoundHashtags(true);
        } catch (err) {
            console.error(err);
        }
    };

    useEffect(() => {
        if (query === "") {
            setResults({});
            setSent(false);
        }
    }, [query]);

    const showRecentUsers = () => {
        if (recentUsers == null || recentUsers.length < 1) return;

        return (
            <div>
                <div>Select from the list of recently signed in users:</div>

                <div
                    style={{
                        margin: ".25rem 0",
                        display: "flex",
                        flexWrap: "wrap",
                        gap: "6px",
                        width: "100%",
                    }}
                >
                    {recentUsers.map((user) => (
                        <div key={user.nickname}>
                            <Link href={`/profile/~${user.nickname}`}>
                                ~{user.nickname}
                            </Link>
                        </div>
                    ))}
                </div>
            </div>
        );
    };

    const showUsers = () => {
        if (!Object.hasOwn(results, "users")) {
            if (sent) return <p>No results found</p>;
            return null;
        }

        return results.users.map((user) => (
            <div key={user.nickname}>
                <Link href={`/profile/~${user.nickname}`}>
                    ~{user.nickname}{" "}
                    {user.first_name &&
                        user.last_name &&
                        `(${user.first_name} ${user.last_name})`}
                </Link>
            </div>
        ));
    };

    const showHashtags = () => {
        if (!Object.hasOwn(results, "hashtags")) {
            if (sent) return <p>No results found</p>;
            return null;
        }

        return results.hashtags.map((hashtag) => {
            return (
                <div key={hashtag.name}>
                    <Link
                        href={route("search.show-hashtag", {
                            hashtag: hashtag.name,
                        })}
                    >
                        #{hashtag.name}
                    </Link>
                </div>
            );
        });
    };

    return (
        <AuthenticatedLayout auth={auth} errors={errors}>
            <Head title="Search" />

            <div style={{ marginTop: "1rem", marginBottom: "5rem" }}>
                <Content size="800px">
                    {showRecentUsers()}

                    <h2>Search</h2>

                    <div
                        style={{
                            display: "flex",
                            gap: ".5rem",
                            margin: ".5rem 0 1rem 0",
                        }}
                    >
                        {hashtags.map((h) => {
                            return (
                                <Link
                                    href={`/search/hashtag/${h.name}`}
                                    key={h.id}
                                >
                                    #{h.name}
                                </Link>
                            );
                        })}
                    </div>

                    <form onSubmit={handleSearch}>
                        <TextInput
                            type="text"
                            name="query"
                            value={query}
                            onChange={(e) => setQuery(e.target.value)}
                            autoFocus
                            placeholder="Tip: to search for hashtags, starts your query with #"
                        />
                        {query != "" && <p>Press Enter to search </p>}
                    </form>

                    <div className="mb-2">
                        {foundHashtags ? showHashtags() : showUsers()}
                    </div>
                </Content>
            </div>
        </AuthenticatedLayout>
    );
}
