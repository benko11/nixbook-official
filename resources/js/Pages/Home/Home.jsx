import React from "react";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, usePage } from "@inertiajs/inertia-react";
import Content from "@/Components/Content";
import TabbedMenu from "../../Components/TabbedMenu";
import TextForm from "./TextForm";
import GalleryForm from "./GalleryForm";
import CodeForm from "./CodeForm";
import ArticleForm from "./ArticleForm";
import Pagination from "@/Components/Pagination";

export default function Home(props) {
    return (
        <AuthenticatedLayout auth={props.auth} errors={props.errors}>
            <Head title="Your Feed" />

            <Content size="800px">
                <div style={{ marginBottom: "5rem" }}>
                    <Pagination
                        auth={props.auth}
                        stashes={props.stashes}
                        emptyMessage={
                            <div>
                                Your feed is empty, you should maybe{" "}
                                <Link href={route("search.index")}>
                                    follow some accounts
                                </Link>
                            </div>
                        }
                        forms={
                            <TabbedMenu
                                className="my-2"
                                items={{
                                    Text: <TextForm />,
                                    Gallery: <GalleryForm />,
                                    Code: (
                                        <CodeForm
                                            codeLanguages={props.codeLanguages}
                                        />
                                    ),
                                    Article: <ArticleForm />,
                                }}
                            />
                        }
                        url="/api/posts/all"
                    />
                </div>
            </Content>
        </AuthenticatedLayout>
    );
}
