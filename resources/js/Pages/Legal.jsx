import GuestLayout from "@/Layouts/GuestLayout";
import { Head, Link, usePage } from "@inertiajs/inertia-react";
import Content from "@/Components/Content";
import AutomaticLayout from "@/Layouts/AutomaticLayout";

export default function Legal({ auth }) {
    const global = usePage();

    return (
        <AutomaticLayout auth={auth}>
            <Head title="Legal stuff" />
            <Content>
                <div className="mt-2">
                    <h1>Legal Stuff</h1>
                    <p>
                        Here are some documents provided for your convenience to
                        inform you about important aspects of{" "}
                        {global.props.appName}:
                    </p>

                    <div className="mb-2">
                        <ul>
                            <li>
                                <Link href={route("legal.privacy-policy")}>
                                    Privacy Policy
                                </Link>
                            </li>
                            <li>
                                <Link href={route("legal.code-of-conduct")}>
                                    Code of Conduct
                                </Link>
                            </li>
                            <li>
                                <Link href={route("legal.licence")}>
                                    Distribution Licence
                                </Link>
                            </li>
                        </ul>
                    </div>

                    <p>
                        As a user of the application, the first two documents
                        are the most important.
                    </p>
                </div>
            </Content>
        </AutomaticLayout>
    );
}
