import Content from "@/Components/Content";
import Emoji from "@/Components/Emoji";
import AutomaticLayout from "@/Layouts/AutomaticLayout";
import ListicleTable from "@/Styles/ListicleTable";
import { Head, Link } from "@inertiajs/inertia-react";

export default function About({ auth }) {
    return (
        <AutomaticLayout auth={auth}>
            <Head title="About" />
            <Content className="mt-2">
                <h1>About</h1>

                <ListicleTable className="my-2">
                    <colgroup>
                        <col className="category" />
                        <col className="value" />
                    </colgroup>

                    <tbody>
                        <tr>
                            <th>Author</th>
                            <td>
                                <Link href="/profile/~benko11">~benko11</Link>
                            </td>
                        </tr>
                        <tr>
                            <th>Testers</th>
                            <td>Mark, Drea, Lukas, Rado</td>
                        </tr>
                        <tr>
                            <th>Special support</th>
                            <td>93pigeons</td>
                        </tr>
                        <tr>
                            <th>Written in</th>
                            <td>ReactJS, Laravel, InertiaJS</td>
                        </tr>
                    </tbody>
                </ListicleTable>
                <p>
                    Made on <Emoji character="🌍" />
                </p>
            </Content>
        </AutomaticLayout>
    );
}
