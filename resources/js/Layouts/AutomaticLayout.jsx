import GuestLayout from "@/Layouts/GuestLayout";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";

export default function AutomaticLayout({ auth, children }) {
    if (auth == null) return;
    if (auth.user == null) return <GuestLayout>{children}</GuestLayout>;

    return <AuthenticatedLayout auth={auth}>{children}</AuthenticatedLayout>;
}
