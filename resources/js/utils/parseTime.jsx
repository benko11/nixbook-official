export function parseTime(seconds) {
    const MINUTE = 60;
    const HOUR = MINUTE * 60;
    const DAY = HOUR * 24;

    if (seconds < MINUTE) {
        return `${seconds} second${seconds > 1 ? "s" : ""}`;
    }

    if (seconds < HOUR) {
        return `${seconds / MINUTE} minute${seconds / MINUTE > 1 ? "s" : ""}`;
    }

    if (seconds < DAY) {
        return `${seconds / HOUR} hour${seconds / HOUR > 1 ? "s" : ""}`;
    }

    return `${seconds / DAY} day${seconds / DAY > 1 ? "s" : ""}`;
}
