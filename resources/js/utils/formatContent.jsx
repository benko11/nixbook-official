import { Link } from "@inertiajs/inertia-react";
import { v4 as uuidv4 } from "uuid";
import {} from "emoji-regex";
import Emoji from "@/Components/Emoji";

export function formatContent(contents, keepString = false, isArticle = false) {
    const isEmoji = (character) => {
        if ("0123456789#*".includes(character)) return false;

        if (isNaN(+contents)) return false;
        return /\p{Emoji}/u.test(character);
    };

    const hashtagPattern = /(#[a-zA-Z0-9]+)(?!\w)/g;
    const hashtagUrl = (p) =>
        route("search.show-hashtag", {
            hashtag: p.substr(1).trim(),
        });
    let parts = contents;
    parts = parts.split(hashtagPattern);

    if (!keepString) {
        parts = parts.flatMap((p) => p.split(/(\n)/));

        parts = parts.map((p) => {
            if (p == null || p === "") return;
            if (p.match(hashtagPattern)) {
                return (
                    <Link key={uuidv4()} href={hashtagUrl(p)}>
                        {p}
                    </Link>
                );
            }

            if (p == "\n") return <br key={uuidv4()} />;

            return p;
        });
    }

    if (!isArticle)
        parts = parts.map((part) => {
            if (typeof part === "object") return part;
            if (typeof part === "string") {
                return part.split(/(\b\w+\b)/g).map((c) => {
                    if (isEmoji(c))
                        return (
                            <Emoji character={c} key={crypto.randomUUID()} />
                        );
                    return c;
                });
            }
        });

    return parts;
}
