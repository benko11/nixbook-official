import styled from "styled-components";

const BottomNavigationList = styled.ul`
    display: flex !important;

    li {
        list-style: none !important;
    }

    li div {
        padding: 2px 8px !important;
    }

    @media (pointer: coarse) {
        a div {
            padding: 0.5rem !important;
        }
    }
`;
export default BottomNavigationList;
