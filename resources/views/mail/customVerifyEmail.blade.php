<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Greetings, <strong>{{ $nickname }}</strong>!</h1>

    <div class="inward">
        <div>We are happy to have you in our community. Please click the link below to verify your email account:</div>

        <div style="padding: .5rem 0"><a href="{{ $url }}" target="_blank">{{ $url }}</a></div>

        <div>(c) <?php echo date('Y'); ?> {{ config('app.name') }}</div>
    </div>
</body>
</html>