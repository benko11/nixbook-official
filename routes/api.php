<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\SearchController;
use App\Models\ColourScheme;
use App\Models\Font;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/fonts', function() {
    return Font::all();
});

Route::get('/colour-schemes', function() {
    return ColourScheme::all();
});

Route::get('/search/hashtags/{query}', [SearchController::class, 'searchHashtags']);
Route::get('/search/users/{query}', [SearchController::class, 'search']);

// Route::get('/posts/all', [PostController::class, 'indexApi'])->name('home-api');

Route::middleware('auth:api')->get('test', function() {
    $user = auth()->user();
    dd($user);
});